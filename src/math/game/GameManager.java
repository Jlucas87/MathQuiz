package math.game;

import java.util.ArrayList;
import math.question.Question;

/**
 * Created by Jacob Lucas on 12/27/16.
 */
public class GameManager {

    private int score;
    private String difficultyLabel = "Difficulty: ";
    public static final int DEFAULT_WEIGHT = 2;
    private ArrayList<Question> questions = null;

    public GameManager() {
        score = 0;
    }

    /**
     *  Updated the score by squaring the level and multiplying it by the provided weight. An operation
     *  is given a higher weight if it's considered more complex.
     *
     * @param level The current difficulty level of the game.
     * @param response True or false - was the question answered correctly?
     * @param weight A multiplier used to increase the score further.
     */
    public void updateScore(int level, boolean response, int weight) {
        if(response) {
            this.score = this.score + (weight * level * level);
        } else {
            this.score = this.score - (level * level);
        }
    }

    /**
     * Update the score using the default weight.
     *
     * @param level The current difficulty level of the game.
     * @param response True or false - was the question answered correctly?
     */
    public void updateScore(int level, boolean response) {
        this.updateScore(level, response, DEFAULT_WEIGHT);
    }

    /**
     * This method creates a label to indicate the current difficulty for the user.
     *
     * @param level The current difficulty level integer
     * @return The difficulty label
     */
    public String getDifficultyString(int level) {
        String label = this.difficultyLabel;
        for(int i = 0; i < level; i++) {
            label = label.concat("@");
        }

        return label;
    }

    /**
     * This function is used to add the current question to the questions list. We track
     * the questions in order for maintaing a history of the user's performance.
     *
     * @param question
     * @return void
     */
    public void addQuestion(Question question) {
        if(this.questions == null) {
            this.questions = new ArrayList<Question>();
        }

        this.questions.add(question);
    }

    public ArrayList<Question> getAllQuestions() {
        return this.questions;
    }

    public int getScore() {
        return this.score;
    }

    public void reset() {
        this.score = 0;
    }
}
