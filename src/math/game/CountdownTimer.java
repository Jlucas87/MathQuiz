package math.game;

import javax.swing.*;
import java.text.DecimalFormat;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by DukeLucas on 1/1/17.
 */
public class CountdownTimer implements Runnable {

    private JLabel timerLabel;
    private float interval;
    private int delay = 0;
    private int period = 100;
    private Timer timer;
    private DecimalFormat format;

    /**
     * This constructor takes in a label to display the countdown timer on and the length of the countdown.
     *
     * @param timerLabel Object to write the current remaining time
     * @param time The total time allotted for the countdown
     */
    public CountdownTimer(JLabel timerLabel, long time){
        this.timerLabel = timerLabel;
        this.interval = time/1000;
        this.timer = new Timer();
        this.format = new DecimalFormat("##.#");
    }

    @Override
    public void run() {
        timer.scheduleAtFixedRate(new TimerTask() {

            public void run() {
                timerLabel.setText("You have "+setInterval()+" seconds remaining");

            }
        }, delay, period);
    }

    private String setInterval() {
        System.out.println(interval);
        if (interval < 0.2)
            timer.cancel();
        interval = interval - 0.1f;
        return format.format(interval);
    }
}
