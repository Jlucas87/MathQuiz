package math.engine;

import math.question.Question;

/**
 * Created by Jacob Lucas on 12/26/16.
 */
public interface MathEngine {

    // This method will return a question based on the parameters entered by the user
    public abstract String getQuestion();

    // This method will check the answer based on the most recently retrieved question
    public abstract String checkAnswer(long answer);

    // A boolean is returned, true if the question was answered correctly
    public abstract boolean getAnswerBool();

    // Return the number of questions the user has answered correctly
    public abstract int getNumCorrect();

    // Return the current difficulty level for the user
    public abstract int getLevel();

    // Return the total responses submitted by the user
    public abstract int getTotalResponses();

    // Return all values to their defaults for a new game
    public abstract void reset();
}
