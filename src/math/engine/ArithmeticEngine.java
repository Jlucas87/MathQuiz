package math.engine;

import java.util.Random;
import math.question.QuestionFactory;
import math.question.Question;

/**
 * Created by Jacob Lucas on 12/26/16.
 */
public class ArithmeticEngine implements MathEngine {

    // The Operation the user would like to see during their quiz
    private Operation[] params;

    // Keep track of the operation of the current question and the answer
    private Question currentQuestion;
    private QuestionFactory qFactory;
    private boolean answerBool;

    // Keep track of the total responses and how many were answered correctly
    private int totalResponses = 0;
    private int numberCorrect = 0;

    // Variables for determining the complexity of the questions
    private int level = 1;
    private int difficultyIncrementor = 0;
    private int modifier = 5;

    /**
     * Default constructor which sets up multiplication and division as the default paramters.
     */
    public ArithmeticEngine() {
        // Set the default paramters if none are provided
        Operation[] defaults = {Operation.MULTIPLICATION, Operation.DIVISION};
        setParameters(defaults);
        this.qFactory = new QuestionFactory();
    }

    public ArithmeticEngine(boolean addition, boolean subtraction) {
        // Call default constructor, but overwrite if addition or subtraction is requested
        this();

        // Add addition and/or subtraction as needed
        if(addition && subtraction) {
            Operation[] options = {Operation.MULTIPLICATION, Operation.DIVISION,
                Operation.ADDITION, Operation.SUBTRACTION};
            this.params = options;
        } else if (addition) {
            Operation[] options = {Operation.MULTIPLICATION, Operation.DIVISION,
                    Operation.ADDITION};
            this.params = options;
        } else if (subtraction) {
            Operation[] options = {Operation.MULTIPLICATION, Operation.DIVISION,
                    Operation.SUBTRACTION};
            this.params = options;
        }
    }

    //////---- Question Generation ----//////

    @Override
    public String getQuestion() {
        this.currentQuestion = null;
        int item = new Random().nextInt(this.params.length);
        Operation op = this.params[item];
        switch (op) {
            case ADDITION:
                this.currentQuestion = getAdditionQuestion();
                break;
            case SUBTRACTION:
                this.currentQuestion = getSubtractionQuestion();
                break;
            case MULTIPLICATION:
                this.currentQuestion = getMultiplicationQuestion();
                break;
            case DIVISION:
                this.currentQuestion = getDivisionQuestion();
                break;
        }

        return this.currentQuestion.buildQuestion();
    }

    protected Question getAdditionQuestion() {
        // Retrieve random operands
        int difficulty = getLevel();
        Random rand = new Random();
        int x = rand.nextInt(difficulty + modifier) + 1;
        int y = rand.nextInt(difficulty + modifier) + 1;
        int a = rand.nextInt(difficulty) + 1;
        int b = rand.nextInt(difficulty) + 1;

        // Generate the question string
        Question mQuestion = qFactory.buildQuestion(new int[]
            {(x * a), (y * b)},
            Operation.ADDITION);

        // Set the answer
        mQuestion.setAnswer((x*a) + (y*b));

        return mQuestion;
    }

    protected Question getSubtractionQuestion() {
        // Retrieve random operands
        int difficulty = getLevel();
        Random rand = new Random();
        int x = rand.nextInt(difficulty + modifier) + 1;
        int y = rand.nextInt(difficulty + modifier) + 1;
        int a = rand.nextInt(difficulty) + 1;
        int b = rand.nextInt(difficulty) + 1;

        // Determine the order based on a positive or zero value solution
        if((x*a) -  (y*b) >= 0) {
            x = x * a;
            y = y * b;

        } else {
            int temp = x;
            x = y * b;
            y = temp * a;
        }

        // Set the question and answer
        Question mQuestion = qFactory.buildQuestion(new int[]
            {x, y},
            Operation.SUBTRACTION);

        // Set the answer
        mQuestion.setAnswer(x - y);

        return mQuestion;
    }

    protected Question getMultiplicationQuestion() {
        // Retrieve random operands
        int difficulty = getLevel();
        Random rand = new Random();
        int x = rand.nextInt(difficulty + modifier) + 1;
        int y = rand.nextInt(difficulty + modifier) + 1;

        // Set the question and answer
        Question mQuestion = qFactory.buildQuestion(new int[]
            {x, y},
            Operation.MULTIPLICATION);

        // Set the answer
        mQuestion.setAnswer(x * y);

        return mQuestion;
    }

    protected Question getDivisionQuestion() {
        // Retrieve random operands
        int difficulty = getLevel();
        Random rand = new Random();
        int x = rand.nextInt(difficulty + modifier) + 1;
        int y = rand.nextInt(difficulty + modifier) + 1;
        int div = x * y;

        // Set the question and answer
        Question mQuestion = qFactory.buildQuestion(new int[]
            {div, y},
            Operation.DIVISION);

        // Set the answer
        mQuestion.setAnswer(div / y);

        return mQuestion;
    }

    ////---- Other Operations ----////

    @Override
    public String checkAnswer(long answer) {
        String result = "";
        if(answer == getAnswer()) {
            this.numberCorrect++;
            this.difficultyIncrementor++;
            if(this.difficultyIncrementor == 4) {
                this.difficultyIncrementor = 1;
                this.level++;
                this.modifier++;
            }
            result = "Correct!";
            this.answerBool = true;
        } else {
            this.difficultyIncrementor--;
            if(this.difficultyIncrementor == 0) {
                this.difficultyIncrementor = 1;
                if(this.level > 1) {
                    this.level--;
                    this.modifier--;
                }
            }
            result = "Incorrect!";
            this.answerBool = false;
        }
        this.totalResponses++;
        return result;
    }

    public void reset() {
        level = 1;
        difficultyIncrementor = 0;
        modifier = 5;
        totalResponses = 0;
        numberCorrect = 0;
    }

    //////---- Getters ----//////

    public long getAnswer() {
        return this.currentQuestion.getAnswer();
    }

    public int getTotalResponses() {
        return totalResponses;
    }

    @Override
    public int getNumCorrect() {
        return this.numberCorrect;
    }

    @Override
    public int getLevel() {
        return this.level;
    }

    public boolean getAnswerBool() {
        return this.answerBool;
    }

    //////---- Setters ----//////

    public void setParameters(Operation[] paramters) {
        this.params = paramters;
    }
}
