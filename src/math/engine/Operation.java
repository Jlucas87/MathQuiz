package math.engine;

/**
 * Created by DukeLucas on 12/29/16.
 */
public enum Operation {
    ADDITION, SUBTRACTION, MULTIPLICATION, DIVISION;
}
