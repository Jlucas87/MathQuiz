package math.engine;

/**
 * Created by Jacob on 3/10/2017.
 */
public class AlgebraEngine implements MathEngine {

    @Override
    public String getQuestion() {
        return null;
    }

    @Override
    public String checkAnswer(long answer) {
        return null;
    }

    @Override
    public boolean getAnswerBool() {
        return false;
    }

    @Override
    public int getNumCorrect() {
        return 0;
    }

    @Override
    public int getLevel() {
        return 0;
    }

    @Override
    public int getTotalResponses() {
        return 0;
    }

    @Override
    public void reset() {

    }
}
