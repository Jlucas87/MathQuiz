package math.ui;

import math.engine.MathEngine;
import math.engine.*;
import math.game.GameManager;

/**
 * Created by Jacob Lucas on 12/26/16.
 */
public class main {

    public static void main(String[] args) {
        MathEngine me = new ArithmeticEngine();
        GameManager gm = new GameManager();
        QuizPanel qp = new QuizPanel(me, gm);
    }
}
