package math.ui;

import math.engine.ArithmeticEngine;
import math.engine.MathEngine;
import math.game.CountdownTimer;
import math.game.GameManager;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * Created by Jacob Lucas on 12/26/16.
 */
public class QuizPanel extends JFrame implements ActionListener, KeyListener{

    private JPanel main = new JPanel(new BorderLayout());
    private JPanel center = new JPanel(new BorderLayout());
    private JPanel centerN = new JPanel(new GridLayout(2,4));
    private JPanel gPan2 = new JPanel(new GridLayout(0,2));
    private JLabel mQuestion = new JLabel();
    private JLabel status = new JLabel();
    private JLabel score1 = new JLabel();
    private JLabel score2 = new JLabel();
    private JLabel timeRemaining = new JLabel();
    private JLabel cLevel = new JLabel();
    private JLabel mainLabel = new JLabel("Welcome to Brain Game Mathematics");
    private JLabel timerLabel = new JLabel("Timer:");
    private JLabel desc = new JLabel("Test your skills with multiplication and division...how many points can you earn!?");
    private JButton start = new JButton("Start Challenge");
    private JButton resp = new JButton("Enter");
    private JPanel gamePanel = new JPanel(new GridLayout(8,0));
    private JTextField ans = new JTextField();
    private JRadioButton min1 = new JRadioButton("1 minute");
    private JRadioButton min2 = new JRadioButton("2 minute");
    private JRadioButton min5 = new JRadioButton("3 minute");
    private ButtonGroup minGroup = new ButtonGroup();
    private JLabel optLabel = new JLabel("Add Variety: ");
    private JCheckBox opt1 = new JCheckBox("Addition");
    private JCheckBox opt2 = new JCheckBox("Subtraction");
    private Font mainFont = new Font("Helvetica", Font.PLAIN, 24);
    private Font font = new Font("Helvetica", Font.PLAIN, 17);
    private Font font2 = new Font("Helvetica", Font.BOLD, 16);
    private Font timerFont = new Font("Helvetica", Font.PLAIN, 15);
    private Font descFont = new Font("Sans Serif", Font.ITALIC, 14);

    private String answ = "";
    private long timeConstant = (long) 5E3;
    private long star = 0;
    private long answer;
    private boolean started = false;

    // The math engine used to create questions, check answers, and track the difficulty level
    private MathEngine engine;

    // The game manager used to keep track of the score and a player's statistics
    private GameManager manager;
    private CountdownTimer timer;

    public QuizPanel(MathEngine engine, GameManager manager){
        setSize(600, 550);
        setTitle("Math Brain Multiplication v1.1");
        this.setContentPane(main);
        main.add(mainLabel, BorderLayout.NORTH);
        mainLabel.setFont(mainFont);
        main.setFont(font);
        main.add(center, BorderLayout.CENTER);
        main.add(start, BorderLayout.SOUTH);
        start.setFont(font);
        main.setBorder(new EmptyBorder(10,10,10,10));
        start.addActionListener(this);
        resp.addActionListener(this);
        min1.addActionListener(this);
        min1.setFont(font);
        min2.addActionListener(this);
        min2.setFont(font);
        min5.addActionListener(this);
        min5.setFont(font);
        opt1.addActionListener(this);
        opt1.setFont(font);
        opt2.addActionListener(this);
        opt2.setFont(font);
        ans.addKeyListener(this);
        center.add(gamePanel);
        gamePanel.add(desc);
        desc.setFont(descFont);
        gamePanel.add(timeRemaining);
        timeRemaining.setFont(timerFont);
        gamePanel.add(mQuestion);
        mQuestion.setFont(font2);
        gamePanel.add(ans);
        gamePanel.add(resp);
        gamePanel.add(gPan2);
        gPan2.add(status);
        status.setFont(timerFont);
        gPan2.add(cLevel);
        cLevel.setFont(font);
        gamePanel.add(score1);
        score1.setFont(timerFont);
        gamePanel.add(score2);
        score2.setFont(timerFont);
        minGroup.add(min1);
        minGroup.add(min2);
        minGroup.add(min5);
        center.add(centerN, BorderLayout.NORTH);
        centerN.add(timerLabel);
        timerLabel.setFont(font);
        centerN.add(min1);
        centerN.add(min2);
        centerN.add(min5);
        centerN.add(optLabel);
        optLabel.setFont(font);
        centerN.add(opt1);
        centerN.add(opt2);
        this.engine = engine;
        this.manager = manager;
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setVisible(true);
    }

    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();

        //////////Minute Adjustments///////////////////////////////////////////
        if (source == min1 && !started){
            timeConstant = (long) 6E4;
        }
        if (source == min2 && !started){
            timeConstant = (long) 12E4;
        }
        if (source == min5 && !started) {
            timeConstant = (long) 18E4;
        }
        if (source == opt1 && started == false) {
            if (opt1.isSelected() && opt2.isSelected()) {
                this.engine = new ArithmeticEngine(true, true);
            } else if (opt1.isSelected()) {
                this.engine = new ArithmeticEngine(true, false);
            }
        }
        if(source == opt2 && started == false) {
            if(opt1.isSelected() && opt2.isSelected()) {
                this.engine = new ArithmeticEngine(true, true);
            } else if (opt2.isSelected()) {
                this.engine = new ArithmeticEngine(false, true);
            }
        }
        ///////////////////////////////////////////////////////////////////////////


        ///////Program Initializer//////////////////////////////////////////////////
        if(source == start){
            // Setup the UI elements
            timeRemaining.setText("");
            cLevel.setText("Difficulty: @");
            started = true;
            mQuestion.setText("");
            status.setText("");
            score1.setText("");
            score2.setText("");
            star = System.currentTimeMillis();
            engine.reset();
            manager.reset();
            timer = new CountdownTimer(timeRemaining, timeConstant);
            timer.run();

            // Retrieve a question and display it to the user
            mQuestion.setText(engine.getQuestion());
            setVisible(false);
            setVisible(true);
            ans.grabFocus();
        }
        /////////////////////////////////////////////////////////////////

        ///////////////////Response Checker///////////////////////////////
        if(source == resp && System.currentTimeMillis() - star < timeConstant && started == true){
            if(System.currentTimeMillis() - star < timeConstant){
                responseChecker();
            }
        }
        ////////////////////////////////////////////////////////////////////

        ///////////////Time Up Procedure/////////////////////////////////////
        else if(started == true&&source == resp ){
            this.timeUpProcedure();
            setVisible(false);
            setVisible(true);
        }
        /////////////////////////////////////////////////////////////////////
    }

    public void keyPressed(KeyEvent arg0) {
        int source = arg0.getKeyCode();
        ///////////////////Response Checker Keyboard///////////////////////////////
        if(source == KeyEvent.VK_ENTER && started == true && System.currentTimeMillis() - star < timeConstant){
            responseChecker();
        }
        //////////////////////////////////////////////////////////////////////////

        ////////////////Time Up Procedure Keyboard/////////////////////////////////
        else if(started == true&&source == KeyEvent.VK_ENTER) {
            this.timeUpProcedure();
            setVisible(false);
            setVisible(true);
        }
        ///////////////////////////////////////////////////////////////////////////////
    }

    protected void timeUpProcedure() {
        mQuestion.setText("");
        status.setText("Time is up!");
        score1.setText("Your total correct answers: "+engine.getNumCorrect()+"/"+engine.getTotalResponses());
        score2.setText("Your score: "+manager.getScore());
        ans.setText("");
        started = false;
        timeRemaining.setText("You have 0.000 seconds remaining");
    }

    ////////////////Response Checker///////////////////////////////////////////////
    protected void responseChecker(){
        answ = ans.getText();
        timeRemaining.setText("You have "+((timeConstant-(System.currentTimeMillis()-star))
                /1000)+" seconds remaining");
        try{
            answer = Long.valueOf(answ);
        }catch(NumberFormatException n){
            status.setText("Numbers Only");
            setVisible(false);
            setVisible(true);
        }

        // Update the score and status
        status.setText(engine.checkAnswer(answer));

        // Update the level indicator and the score
        cLevel.setText(manager.getDifficultyString(engine.getLevel()));
        manager.updateScore(engine.getLevel(), engine.getAnswerBool());

        // Retrieve the next question
        mQuestion.setText(engine.getQuestion());
        ans.setText("");
        setVisible(false);
        setVisible(true);
    }
    ///////////////////////////////////////////////////////////////////////////////

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyReleased(KeyEvent e) {

    }
}

