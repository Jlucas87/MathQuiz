package math.question;

/**
 * Created by jlucas on 3/3/2017.
 */
public interface Question {

    public abstract void setAnswer(int answ);

    public abstract int getAnswer();

    public abstract String buildQuestion();


}
