package math.question.arithmetic;

import math.question.Question;

/**
 * Created by jlucas on 3/3/2017.
 */
public class DivisionQuestion implements Question {

    private int answ;
    private int[] operands;

    public DivisionQuestion(int a, int b) {
        this.operands = new int[] {a, b};
    }

    public DivisionQuestion(int[] operands) {
        if(operands.length > 0) {
            this.operands = operands;
        }
    }

    @Override
    public void setAnswer(int answ) {
        this.answ = answ;
    }

    @Override
    public int getAnswer() {
        return this.answ;
    }

    @Override
    public String buildQuestion() {
        StringBuilder sb = new StringBuilder();
        sb.append("What is ");

        // Add the operands to the question
        for(int i = 0; i < this.operands.length; i++) {
            sb.append(this.operands[i]);
            if(i != (this.operands.length - 1)) {
                sb.append(" / ");
            }
        }
        sb.append("?");

        return sb.toString();
    }
}
