package math.question;

import math.engine.Operation;
import math.question.arithmetic.*;

/**
 * Created by jlucas on 3/3/2017.
 */
public class QuestionFactory {

    public Question buildQuestion(int[] params, Operation op) {
        Question quest = null;
        switch (op) {
            case ADDITION:
                quest = new AdditionQuestion(params);
                break;
            case SUBTRACTION:
                quest = new SubtractionQuestion(params);
                break;
            case MULTIPLICATION:
                quest = new MultiplicationQuestion(params);
                break;
            case DIVISION:
                quest = new DivisionQuestion(params);
                break;
        }

        return quest;
    }
}
